﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SMTimer : MonoBehaviour {

    public Text m_TimerTxt;
    private float m_StartTime;
    
	void Start () {
        m_StartTime = Time.time;
	}
	
	// Update is called once per frame
	void Update () {
        float t = Time.time - m_StartTime;

        string l_Min = ((int)t / 60).ToString();
        string l_Sec = (t % 60).ToString("F2");

        m_TimerTxt.text = l_Min + ":" + l_Sec;
	}
}
