﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class checkerTest : MonoBehaviour {

    public GameState m_GameState;
    public GameObject m_Ball;
    //public GameObject m_Light;

    public int m_ScoreValue;

    public Light m_Light;

    // Use this for initialization
    void Start ()
    {
        m_Light.enabled = false;
	}
	
	// Update is called once per frame
	void Update ()
    {
        
    }
    void OnTriggerEnter(Collider _col)
    {
        if (_col.gameObject.name == m_Ball.gameObject.name)
        {
            m_GameState.addScore(m_ScoreValue);
            m_Light.enabled=!m_Light.enabled;
        }
    }
}
