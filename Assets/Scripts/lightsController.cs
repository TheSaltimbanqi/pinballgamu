﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class lightsController : MonoBehaviour {

    [Header("Lights List")]
    public List<Light> m_Lights = new List<Light>();
    [Header("All Lights On Check")]
    public bool m_AllReady = false;
    public bool m_CheckLights = true;
    [Header("Flashing Lights")]
    public float m_FlashTime;
    public int m_FlashTimes;
    public int m_FlashCount;
    public bool m_isFlashing;

    float m_Timer;

    // Use this for initialization
    void Start ()
    {
        m_CheckLights = true;

        
    }
	
	// Update is called once per frame
	void Update ()
    {
        //m_AllReady = ;

        if (isAllLightsEnabled())
        {
            Debug.LogError("LightsOn!");
            if (!m_isFlashing)
            {
                m_isFlashing = true;
                m_Timer = Time.time;
            }
            float l_Time2 = Time.time - m_Timer;
            float l_Second = (l_Time2 % 60);
            Debug.LogError(l_Second);


            StartCoroutine(LightsOn());
        }
        if (Input.GetKey(KeyCode.I))
        {
            for (int i = 0; i < m_Lights.Count; ++i)
            {
                m_Lights[i].enabled = false;
            }
        }
    }

    bool isAllLightsEnabled()
    {
        int lightsOn = 0;

        bool l_On=false;
        List<bool> l_OnLights = new List<bool>();

        for (int i = 0; i < m_Lights.Count; ++i)
        {
            if (m_Lights[i].enabled && !m_isFlashing)
            {
                l_OnLights.Add(m_Lights[i].enabled);
                lightsOn++;
            }
        }
        if(l_OnLights.Count == m_Lights.Count)
        {
            l_On = true;
            m_CheckLights = false;
            m_Timer = Time.time;
        }
        Debug.Log("There is " + lightsOn + " enabled of " + m_Lights.Count);
        return l_On;
    }

    IEnumerator LightsOn()
    {
        m_isFlashing = true;
        Debug.LogError("Lights!");
        for (int i = 0; i < m_Lights.Count; ++i)
        {
            m_Lights[i].enabled = !m_Lights[i].enabled;

        }
        yield return new WaitForSeconds(m_FlashTime);
        for (int i = 0; i < m_Lights.Count; ++i)
        {
            m_Lights[i].enabled = !m_Lights[i].enabled;
        }
        yield return new WaitForSeconds(m_FlashTime);
        yield return null;
    }
}
