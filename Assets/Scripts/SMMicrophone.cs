﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SMMicrophone : MonoBehaviour {

    public GameObject m_Ball;

    public Animator m_Anim;
    public AnimationClip m_MicroUp;
    public AnimationClip m_MicroDown;

    public SMMicroDetectCollision m_Collider;
    public bool m_IsDown=false;
    
    //1 Waiting - 2 Down - 3 Up
    public int m_State=1;

	// Use this for initialization
	void Start () {
        m_Anim = this.GetComponent<Animator>();
        m_IsDown = false;
        m_Anim.enabled = false;
	}
	
	// Update is called once per frame
	void Update ()
    {
        m_IsDown = m_Collider.getDown();
        if(m_IsDown)
        {
            m_Anim.enabled = true;
            m_Anim.Play(m_MicroDown.name);
            m_State = 2;
        }
        else if(!m_IsDown && m_State==2)
        {
            m_Anim.Play(m_MicroUp.name);
            m_State = 3;
        }
	}    
}
