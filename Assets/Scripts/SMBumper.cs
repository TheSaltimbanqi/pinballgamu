﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SMBumper : MonoBehaviour {

    public GameState m_GameState;
    public GameObject m_Ball;
    public int m_BoundForce;
    public int m_ScoreValue;

    public AudioSource m_Audio;
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnCollisionEnter(Collision _col)
    {
        if (_col.gameObject.name == m_Ball.gameObject.name)
        {
            //this.gameObject.GetComponent<MeshRenderer>().material.color = Color.red;
            m_GameState.addScore(m_ScoreValue);

            Vector3 l_Direction = (_col.contacts[0].point - m_Ball.transform.position);
            l_Direction = -l_Direction.normalized;
            m_Ball.gameObject.GetComponent<Rigidbody>().AddForce(l_Direction * m_BoundForce);

            m_Audio.Play();
        }
    }
}
