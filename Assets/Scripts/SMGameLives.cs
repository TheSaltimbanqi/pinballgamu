﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SMGameLives : MonoBehaviour {

    public Text m_Lives;
    public GameState m_GameManager;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        m_Lives.text = m_GameManager.getLives().ToString();
    }
}
