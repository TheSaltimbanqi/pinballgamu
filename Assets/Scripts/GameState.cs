﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameState : MonoBehaviour {

    [Header("Score")]
    public int m_Score=0;
    [Header("Ball and Lives")]
    public GameObject m_Ball;
    public GameObject m_BallStartPosition;
    public bool m_BallWaiting = false;
    public int m_Lives = 3;
    public bool m_GameOver;
    public Text m_GameOverText;
    [Header("No return door")]
    public noReturnDoor m_NoReturnDoor;

    public

	// Use this for initialization
	void Start () {
        m_Ball.transform.position = m_BallStartPosition.transform.position;
        m_BallWaiting = false;
        m_GameOver = false;
    }
	
	// Update is called once per frame
	void Update () {
        Debug.Log("Score: " + m_Score);
        if (Input.GetKey(KeyCode.R))
        {
            restartGameTry();
            m_NoReturnDoor.resetDoor();
        }
        if ((Input.GetKey(KeyCode.Space)) && m_BallWaiting && isPlayerAlive())
        {
            restartGameTry();
            m_BallWaiting = false;
            m_NoReturnDoor.resetDoor();
        }
        if (Input.GetKey(KeyCode.T))
        {
            m_NoReturnDoor.resetDoor();
        }
    }

    public void setScore(int _score)
    {
        m_Score = _score;
    }
    public void addScore(int _score)
    {
        m_Score += _score;
    }
    public int getScore()
    {
        return m_Score;
    }

    public void setBallPosition(Vector3 _pos)
    {
        m_Ball.transform.position = _pos;
    }
    public Vector3 getStartPositionBall()
    {
        return m_BallStartPosition.transform.position;
    }
    public void restartGameTry()
    {
        setBallPosition(getStartPositionBall());
    }
    public bool getIsBallWaiting()
    {
        return m_BallWaiting;
    }
    public void setBallWaiting(bool _waiting)
    {
        m_BallWaiting = _waiting;
    }
    public bool isPlayerAlive()
    {
        if(m_Lives>0)
        {
            return true;
            m_GameOverText.text = "";
        }
        else
        {
            m_GameOver = true;
            return false;
            m_GameOverText.text="GAME OVER";
        }
    }

    public void setNoReturnDoorDown()
    {
        m_NoReturnDoor.DoorDown();
    }
    public void setLives(int _lives)
    {
        m_Lives = _lives;
    }
    public int getLives()
    {
        return m_Lives;
    }
}
