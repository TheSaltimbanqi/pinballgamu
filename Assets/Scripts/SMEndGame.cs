﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SMEndGame : MonoBehaviour {

    [Header("Ball")]
    public GameObject m_Ball;
    public GameObject m_BallPosition;
    [Header("Game state")]
    public GameState m_GameState;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    
    void OnCollisionEnter(Collision _col)
    {
        m_GameState.m_Lives--;
        m_Ball.transform.position = m_BallPosition.transform.position;
        m_GameState.setBallWaiting(true);
        m_GameState.setNoReturnDoorDown();
    }
}
