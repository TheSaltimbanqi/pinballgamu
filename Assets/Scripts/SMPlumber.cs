﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SMPlumber : MonoBehaviour {

    [Header("Flipper Position")]
    public float restPosition = 0f;
    public float pressedPossition = 45f;
    [Header("Flipper force")]
    public float hitStrenght = 10000f;
    public float flipperDamper = 150f;
    [Header("Ball")]
    public GameObject m_Ball;
    public float m_BallForce;
    HingeJoint hinge;
    [Header("")]
    public string letter;

    // Use this for initialization
    void Start()
    {
        hinge = GetComponent<HingeJoint>();
        hinge.useSpring = true;
    }

    // Update is called once per frame
    void Update()
    {

        JointSpring spring = new JointSpring();
        spring.spring = hitStrenght * 15;

        spring.damper = flipperDamper;

        if (Input.GetAxis(letter) == 1)
        {
            spring.targetPosition = pressedPossition;
        }
        else
        {
            spring.targetPosition = restPosition;
        }

        hinge.spring = spring;
        hinge.useLimits = true;
    }
    private void OnCollisionExit(Collision _col)
    {
        _col.gameObject.GetComponent<Rigidbody>().AddForce(0, 0, m_BallForce);
    }
}
