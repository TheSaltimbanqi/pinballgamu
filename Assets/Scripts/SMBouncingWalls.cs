﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SMBouncingWalls : MonoBehaviour {

    public GameObject m_Ball;
    public int m_BoundForce;

    public AudioSource m_Audio;
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnCollisionEnter(Collision _col)
    {
        if (_col.gameObject.name == m_Ball.gameObject.name)
        {
            Vector3 l_Direction = (_col.contacts[0].point - m_Ball.transform.position);
            l_Direction = -l_Direction.normalized;
            m_Ball.gameObject.GetComponent<Rigidbody>().AddForce(l_Direction * m_BoundForce);
        }
        m_Audio.Play();
    }
}
