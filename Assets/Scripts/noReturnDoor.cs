﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class noReturnDoor : MonoBehaviour {

    public GameObject m_Ball;
    public Collider m_Collider;
    public Renderer m_Render;
    public Animator m_Animation;
    public AnimationClip m_AnimClipUp;
    public AnimationClip m_AnimClipDown;
    public bool m_BallExit;

    public Vector3 m_StartPosition;

    public AudioSource m_Sound;

	// Use this for initialization
	void Start ()
    {
        m_StartPosition = this.gameObject.transform.position;

        m_Collider = this.GetComponent<Collider>();
        m_Render = this.GetComponent<Renderer>();
        m_Render.enabled = false;
        m_Animation = this.GetComponent<Animator>();
        m_Animation.enabled = false;
	}
	
	// Update is called once per frame
	void Update ()
    {
        if(Input.GetKey(KeyCode.Q))
        {
            DoorDown();
        }
	}
    
    private void OnTriggerExit(Collider _col)
    {
        m_Animation.Rebind();
        if (m_Ball.gameObject.name == _col.gameObject.name && !m_BallExit)
        {
            m_BallExit = true;
            m_Collider.isTrigger = false;
            m_Render.enabled = true;

            m_Animation.enabled = true;
            m_Animation.Play(m_AnimClipUp.name);
            m_Sound.Play();
        }
    }

    public void DoorDown()
    {
        m_Animation.Play(m_AnimClipDown.name);
        m_BallExit = false;
        m_Collider.isTrigger = true;
    }
    public void resetDoor()
    {
        //m_Animation.Play(m_AnimClipUp.name);
        this.gameObject.transform.position = m_StartPosition;
        m_Render.enabled = false;
        m_BallExit = false;
        m_Collider.isTrigger = true;
    }
}
