﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SMMicroDetectCollision : MonoBehaviour {

    public GameObject m_Ball;
    public int m_BoundForce;
    public bool m_IsDown;

    [Header("Score")]
    public GameState m_GameState;
    public int m_ScoreValue;

    public AudioSource m_Sound;
    // Use this for initialization
    void Start ()
    {                
        m_Sound = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update () {
		
	}

    private void OnCollisionEnter(Collision _col)
    {
        if (_col.gameObject.name == m_Ball.gameObject.name)
        {
            if (m_IsDown)
            {
                m_IsDown = false;
            }
            else
            {
                m_IsDown = true;
            }
            m_GameState.addScore(m_ScoreValue);

            Vector3 l_Direction = (_col.contacts[0].point - m_Ball.transform.position);
            l_Direction = -l_Direction.normalized;
            m_Ball.gameObject.GetComponent<Rigidbody>().AddForce(l_Direction * m_BoundForce);
        }
        m_Sound.Play();
    }

    public bool getDown()
    {
        return m_IsDown;
    }
}
